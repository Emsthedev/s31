const http = require('http');

const port = 8000;

const server = http.createServer(function(request,response){
            if (request.url == '/greeting') {
                //You can use request .url to get the current destionation of
                // the user in the browser. You can also check if the current destination of
                //the user matchers the end point and if it does the respective process
                //for each endpoint
                response.writeHead(200, {'Content-Type': 'text/plain'})
                response.end('Hello Batch-197PT')
            } else if(request.url == '/homepage'){
                response.writeHead(200, {'Content-Type': 'text/plain'})
                response.end('Welcome to homepage')
            }else{
                //if none of the endpoint match the current destination(url) of the user
                //return a default value "Page Not Found"
                response.writeHead(404, {'Content-Type': 'text/plain'})
                response.end('Page Not Found')
            }

})
server.listen(port);
//7. Let the terminal/console know that the server is running on which port
console.log(`Server is now accessible at localhost: ${port}`);